import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { UserService } from '../../services/user/user.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-signup',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  form: FormGroup
  isConfirmed: boolean = false
  isLoading: boolean = false

  constructor(
    private fb: FormBuilder,
    private service: UserService,
    private router: Router
  ) {
    this.createFormData()
  }

  ngOnInit(): void {}

  private createFormData(): void {
    this.form = this.fb.group({
      firstName: ['Sila', Validators.required],
      lastName: ['Setthakan-anan', Validators.required],
      username: [
        'fResultSila',
        [Validators.required, Validators.pattern(/^[A-z0-9]{8,16}$/)]
      ],
      password: ['!Secret#Password@123', Validators.required],
      isConfirmed: [true]
    })
  }

  onRegister(): void {
    this.form.get('firstName').markAsTouched()
    this.form.get('lastName').markAsTouched()
    this.form.get('username').markAsTouched()
    this.form.get('password').markAsTouched()
    if (this.form.invalid) return

    this.isLoading = true

    const {
      firstName,
      lastName,
      username,
      password,
      isConfirmed
    } = this.form.value

    this.service
      .register(
        {
          id: Math.floor(Math.random() * 100),
          firstName,
          lastName,
          username,
          password
        },
        isConfirmed
      )
      .subscribe(
        (user) => {
          this.isLoading = false
          alert('✔ Successfully: ลงทะเบียนผู้ใช้สำเร็จ!')
          this.router.navigate(['/', 'sign-in']).then()
        },
        (error) => {
          alert(error.message)
          this.isLoading = false
        }
      )
  }
}
