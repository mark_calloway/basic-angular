import { Component, OnInit } from '@angular/core'
import { Service2Service } from '../../services/service2.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(private service2: Service2Service) {}

  ngOnInit(): void {
    this.service2.formDataSubject.subscribe((data) => console.log({ data }))
  }
}
