import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, Deactivate {
  constructor() {}

  ngOnInit(): void {}

  onExit(): boolean {
    return confirm('Are you sure to exit this page ?')
  }
}
