import { Directive, HostBinding, HostListener, OnInit } from '@angular/core'

@Directive({
  selector: '[appDirective1]'
})
export class Directive1Directive implements OnInit {
  constructor() {}

  @HostBinding('style.color')
  color: string

  @HostListener('mouseover')
  onMouseOver(): void {
    this.color = '#FF0000'
  }

  @HostListener('mouseout')
  onMouseOut(): void {
    this.color = '#000000'
  }

  ngOnInit(): void {
  }
}
