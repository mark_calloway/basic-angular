export interface IFormData {
  sex: string
  firstName: string
  lastName: string
  phones: Array<string>
}
