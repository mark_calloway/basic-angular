import { Component } from '@angular/core'
import { UserService } from '../services/user/user.service'

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent {
  beautifiedUsers: string = ''

  constructor(private service: UserService) {
    service.getUsers().subscribe(
      (users) => {
        this.beautifiedUsers = JSON.stringify(users, undefined, 3)
      },
      (error) => alert(`❌ Error: ${error.message}`)
    )
  }
}
