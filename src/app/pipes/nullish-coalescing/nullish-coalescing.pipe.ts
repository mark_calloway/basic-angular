import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'nullishCoalescing'
})
export class NullishCoalescingPipe implements PipeTransform {
  transform<T>(value: T): T | string {
    return value ?? ''
  }
}
