import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { IUser } from '../../interfaces/IUser'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  users: IUser[] = []

  constructor() {}

  register(user: IUser, isConfirmed: boolean): Observable<IUser> {
    return new Observable((observe) => {
      setTimeout(() => {
        if (this.checkIsUsernameExist(user)) {
          observe.error(
            new Error(`❌ Error: ชื่อผู้ใช้ ${user.username} ถูกใช้งานแล้ว`)
          )
          return
        }
        if (isConfirmed) {
          this.users = [...this.users, user]
          observe.next(user)
        }
      }, 1500)
    })
  }

  login(username: string, password: string): Observable<boolean> {
    return new Observable<boolean>((observe) => {
      setTimeout(() => {
        console.log(this.checkLogInSucceed(username, password), 'hello')
        this.checkLogInSucceed(username, password)
          ? observe.next(true)
          : observe.error(new Error('Username หรือ Password ไม่ถูกต้อง'))
      }, 1500)
    })
  }

  getUsers(): Observable<Array<IUser>> {
    return new Observable<Array<IUser>>((observe) => {
      setTimeout(() => {
        observe.next(
          this.users.map((user) => ({
            ...user,
            password: '•'.repeat(user.password.length)
          }))
        )
      }, 1500)
    })
  }

  displayUserLoggedIn(): IUser {
    return { id: 1, firstName: '', lastName: '', username: '', password: '' }
  }

  checkIsUsernameExist(user): boolean {
    return this.users.some((u) => user.username === u.username)
  }

  checkLogInSucceed(username, password): boolean {
    return this.users.some((u) => {
      return u.username === username && u.password === password
    })
  }
}
