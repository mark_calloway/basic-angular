type BasicFormData = {
  text: string
  num: number
  date: string
  selected: string
  checked1: boolean
  checked2: boolean
  radio: string
  textarea: string
  multipleSelected: Array<string>
  fileUploaded: string
}
