import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { HomeComponent } from 'app/components/home/home.component'
import { SignInComponent } from 'app/components/signin/sign-in.component'
import { SignUpComponent } from 'app/components/signup/sign-up.component'
import { ProfileComponent } from 'app/components/profile/profile.component'
import { DataComponent } from 'app/data/data.component'
import { AuthenticationGuard } from './guards/authentication/authentication.guard'

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'sign-in', component: SignInComponent },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthenticationGuard]
  },
  { path: 'data', component: DataComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule {}
